# -*- coding: utf-8 -*-
# EDIS - a simple cross-platform IDE for C
#
# This file is part of Edis
# Copyright 2014-2015 - Gabriel Acosta <acostadariogabriel at gmail>
# License: GPLv3 (see http://www.gnu.org/licenses/gpl.html)

import sys
from PyQt5.QtGui import QApplication, QtWidgets
qApp = QApplication(sys.argv)
import src  # lint:ok
