# -*- coding: utf-8 -*-
# EDIS - a simple cross-platform IDE for C
#
# This file is part of Edis
# Copyright 2014-2015 - Gabriel Acosta <acostadariogabriel at gmail>
# License: GPLv3 (see http://www.gnu.org/licenses/gpl.html)

from PyQt5.QtGui import QDockWidget, QtWidgets

from PyQt5.QtCore import Qt


class CustomDock(QDockWidget):

    def __init__(self):
        super().__init__()
        # Siempre undock/redock a la izquierda o derecha
        self.setAllowedAreas(Qt.LeftDockWidgetArea | Qt.RightDockWidgetArea)
        self.setMaximumWidth(356)
